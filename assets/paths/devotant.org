*** Devotant
God's Name:

Stat:

- [-] When you say your daily prayers, you may [[file:~/other/ironsworn/moves/adventure/secure-advantage.org][Secure an Advantage]] by asking your god to grant a blessing. If you do, roll +your god's stat. On a hit, take +2 momentum.

- [ ] When you [[file:~/other/ironsworn/moves/quest/swear-an-iron-vow.org][Swear an Iron Vow]] to serve your god on a divine quest, you may roll +your god's stat and reroll any dice. When you [[file:~/other/ironsworn/moves/quest/fulfill-your-vow.org][Fulfill Your Vow]] and mark experience, take +1 experience.

- [ ] When you [[file:~/other/ironsworn/moves/relationship/sojourn.org][Sojourn]] by sharing the word of your god, you may roll +your god's stat. If you do, take +1 momentum on a hit.
