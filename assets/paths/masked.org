*** Masked
Once you mark a bond with elves, and are gifted a mask of precious elderwood...

- [-] Choose your mask's material.
    - Thunderwood: Edge / Health
    - Bloodwood: Iron / Health
    - Ghostwood: Shadow / Spirit
    - Whisperwood: Wits / Spirit

  When you wear the mask and make a move which uses its stat, add +1. If you roll a 1 on your action die, suffer -1 to the associated track (in addition to any other outcome of the move).

- [ ] As above, and you may instead add +2 and suffer -2 (decide before rolling).

- [ ] When you [[file:~/other/ironsworn/moves/suffer/face-death.org][Face Death]] or [[file:~/other/ironsworn/moves/suffer/face-desolation.org][Face Desolation]] while wearing the mask, you may roll +its stat (instead of +heart).

