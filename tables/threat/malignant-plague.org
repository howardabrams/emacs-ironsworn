#+TITLE:  Threat: Malignant Plague
#+AUTHOR: Howard X. Abrams
#+EMAIL:  howard.abrams@gmail.com
#+DATE:   2021-12-27 December
#+TAGS:   rpg ironsworn

  - Manifest new symptoms or effects
  - Infect someone important
  - Expand to new territory or communities
  - Allow someone to take advantage
  - Allow someone to take the blame
  - Create panic or disorder
  - Force a horrible decision
  - Lure into complacency
  - Reveal the root of the sickness
  - Undermine a potential cure
