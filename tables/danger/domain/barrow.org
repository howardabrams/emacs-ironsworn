#+TITLE: Dangers in a Barrow

 - Denizen guards this area
 - Trap
 - Death makes its presence known
 - Crumbling architecture
 - Grave goods with hidden dangers
