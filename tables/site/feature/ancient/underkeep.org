#+TITLE: Features in an Ancient Underkeep


Roll on Table: d100
  |   1-4 | Evidence of lost knowledge            |
  |   5-8 | Inscrutable relics                    |
  |  9-12 | Ancient artistry or craft             |
  | 13-16 | Preserved corpses or fossils          |
  | 17-20 | Visions of this place in another time |
  | 21-43 | Carved passages                       |
  | 44-56 | Hall or chamber                       |
  | 57-64 | Stairs into the depths                |
  | 65-68 | Grand doorway or entrance             |
  | 69-72 | Tomb or catacombs                     |
  | 73-76 | Rough-hewn cave                       |
  | 77-80 | Foundry or workshop                   |
  | 81-84 | Shrine or temple                      |
  | 85-88 | Imposing architecture or artistry     |
  | 89-98 | Something unusual or unexpected       |
  |    99 | You transition into a new theme       |
  |   100 | You transition into a new domain      |
