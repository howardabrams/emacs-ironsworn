#+TITLE: Features in an Infested Location

Roll on Table: d20
  |   1-4 | Inhabited nest                        |
  |   5-8 | Abandoned nest                        |
  |  9-12 | Ravaged terrain or architecture       |
  | 13-16 | Remains or carrion                    |
  | 17-20 | Hoarded food                          |
#+TITLE: Features of an Underkeep

Roll on Table: d80 + 20
  | 21-43 | Carved passages                   |
  | 44-56 | Hall or chamber                   |
  | 57-64 | Stairs into the depths            |
  | 65-68 | Grand doorway or entrance         |
  | 69-72 | Tomb or catacombs                 |
  | 73-76 | Rough-hewn cave                   |
  | 77-80 | Foundry or workshop               |
  | 81-84 | Shrine or temple                  |
  | 85-88 | Imposing architecture or artistry |
  | 89-98 | Something unusual or unexpected   |
  |    99 | You transition into a new theme   |
  |   100 | You transition into a new domain  |
