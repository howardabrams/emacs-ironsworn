#+TITLE: Features of a Wild Place

Roll on Table: d20
  |   1-4 | Denizen’s lair                         |
  |   5-8 | Territorial markings                   |
  |  9-12 | Impressive flora or fauna              |
  | 13-16 | Hunting ground or watering hole        |
  | 17-20 | Remains or carrion                     |
#+TITLE: Features of an Underkeep

Roll on Table: d80 + 20
  | 21-43 | Carved passages                   |
  | 44-56 | Hall or chamber                   |
  | 57-64 | Stairs into the depths            |
  | 65-68 | Grand doorway or entrance         |
  | 69-72 | Tomb or catacombs                 |
  | 73-76 | Rough-hewn cave                   |
  | 77-80 | Foundry or workshop               |
  | 81-84 | Shrine or temple                  |
  | 85-88 | Imposing architecture or artistry |
  | 89-98 | Something unusual or unexpected   |
  |    99 | You transition into a new theme   |
  |   100 | You transition into a new domain  |
